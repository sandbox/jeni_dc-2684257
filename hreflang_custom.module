<?php

/**
 * @file
 * Module file for hreflang_custom.
 */

/**
 * Implements hook_permission().
 */
function hreflang_custom_permission() {
  return array(
    'administer hreflang custom' => array(
      'title' => t('Administer hreflang custom module'),
      'description' => t('Configure content types and fields for hreflang custom module.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function hreflang_custom_menu() {
  $items['admin/config/search/hreflang-custom'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hreflang_custom_form'),
    'access arguments' => array('administer hreflang custom'),
    'title' => 'Hreflang custom',
  );
  return $items;
}

/**
 * Configuration form.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function hreflang_custom_form() {
  $form['hreflang_custom_content_types'] = array(
    '#title' => t('Content types'),
    '#type' => 'checkboxes',
    '#options' => node_type_get_names(),
    '#weight' => 1,
    '#description' => 'Select the content types to enable custom hreflang tags.',
    '#default_value' => variable_get('hreflang_custom_content_types', array()),
  );
  $fields = field_info_field_map();
  $list_fields = array();
  foreach ($fields as $field => $info) {
    if ($info['type'] == 'list_text') {
      $list_fields[$field] = $field;
    }
  }
  $form['hreflang_custom_country_field'] = array(
    '#title' => t('Country field'),
    '#type' => 'select',
    '#options' => $list_fields,
    '#required' => TRUE,
    '#empty_option' => '- Select -',
    '#weight' => 2,
    '#description' => 'The field storing the country specific information.',
    '#default_value' => variable_get('hreflang_custom_country_field', array()),
  );
  $form['#submit'][] = 'hreflang_custom_form_submit';
  $country_field = variable_get('hreflang_custom_country_field', array());
  if (!empty($country_field)) {
    $form['hreflang_custom_country_mapping'] = array(
      '#type' => 'fieldset',
      '#title' => t('Country mapping'),
      '#description' => 'Set the hreflang value for each country option.',
      '#weight' => 3,
      '#tree' => TRUE,
    );
    $field = field_info_field($country_field);
    $allowed_values = list_allowed_values($field);
    $mapping_values = variable_get('hreflang_custom_country_mapping', array());
    foreach ($allowed_values as $value => $name)  {
      $form['hreflang_custom_country_mapping'][$value] = array(
        '#title' => $name,
        '#type' => 'select',
        '#options' => array(
          'en-gb' => 'en-gb',
          'en-au' => 'en-au',
          'en-za' => 'en-za',
          'en-ie' => 'en-ie',
          'en-nz' => 'en-nz',
          'en-ca' => 'en-ca',
          'en-us' => 'en-us',
        ),
        '#required' => TRUE,
        '#empty_option' => '- Select -',
        '#default_value' => (isset($mapping_values[$value]) ? $mapping_values[$value] : array()),
      );
    }
  }

  return system_settings_form($form);
}

/**
 * Submit handler for hreflang_custom_form().
 */
function hreflang_custom_form_submit($form, &$form_state) {
  // Add or remove the hreflang_custom field instance.
  $types = drupal_array_diff_assoc_recursive($form_state['values']['hreflang_custom_content_types'], $form['hreflang_custom_content_types']['#default_value']);
  foreach ($types as $type => $value) {
    if ($value) {
      hreflang_custom_add_instance($value);
    }
    else {
      hreflang_custom_delete_instance($type);
    }
  }
}

/** 
 * Add hreflang entity reference field instance.
 *
 * @param $type
 *    Machine name of the content type to add the field.
 */
function hreflang_custom_add_instance($type) {
  if (!field_info_instance('node', 'hreflang_custom_ref', $type)) {
    hreflang_custom_field_instance($type);
  }
}

/**
 * Delete hreflang entity reference field instance.
 *
 * @param $type
 *    Machine name of the content type to add the field.
 */
function hreflang_custom_delete_instance($type) {
  $instance = field_info_instance('node', 'hreflang_custom_ref', $type);
  if ($instance) {
    field_delete_instance($instance, FALSE);
  }
}

/**
 * Entity reference field instance.
 * 
 * @param $type
 *    Machine name of the content type to add the field.
 */
function hreflang_custom_field_instance($type) {
  $instance = array(
    'bundle' => $type,
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select nodes to output as alternate languages.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'hreflang_custom_ref',
    'label' => 'Hreflang custom',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 50,
    ),
  );
  field_create_instance($instance);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function hreflang_custom_form_field_ui_field_overview_form_alter(&$form, &$form_state, $form_id) {
  // Hide delete link in field ui, we want to remove the field instance on our config page.
  if (isset($form['fields']['hreflang_custom_ref'])) {
    $form['fields']['hreflang_custom_ref']['delete']['#access'] = FALSE;
  }
}

/**
 * Implements template_preprocess_html().
 */
function hreflang_custom_preprocess_html(&$variables) {
  $node = menu_get_object();
  $country_field = variable_get('hreflang_custom_country_field');
  if ($node && !empty($node->hreflang_custom_ref) && !empty($node->$country_field)) {
    $wrapper = entity_metadata_wrapper('node', $node);
    
    // Add in the current node.
    $metas = array(
      $wrapper->$country_field->value() => $node->nid,
    );
    
    // Add the referenced nodes.
    foreach ($wrapper->hreflang_custom_ref->getIterator() as $delta => $ref_wrapper) {
      $metas[$ref_wrapper->$country_field->value()] = $ref_wrapper->getIdentifier();
    }
    
    // Add the meta tags.
    $mapping_values = variable_get('hreflang_custom_country_mapping', array());
    if (!empty($mapping_values)) {
      foreach ($metas as $country => $nid) {
        $attributes = array(
          'href' => hreflang_custom_prepare_path('node/' . $nid),
          'rel' => 'alternate',
          'hreflang' => $mapping_values[$country],
        );
        drupal_add_html_head_link($attributes);
      }
    }
  }
}

/**
 * Prepare the full path to output in the meta tag.
 * @param $path
 *    The unaliased path to the node.
 */
function hreflang_custom_prepare_path($path) {
  // Check if node is front page.
  if ($path == variable_get('site_frontpage', 'node')) {
    $path = '<front>';
  }
  // Check if node is front page for a domain access domain.
  elseif (module_exists('domain_conf')) {
    $domains = domain_domains();
    $domains_front = array();
    foreach ($domains as $domain) {
      $domains_front[$domain['domain_id']] = domain_conf_variable_get($domain['domain_id'], 'site_frontpage');
    }
    if ($key = array_search($path, $domains_front)) {
      $path = $domains[$key]['path'];
    }
  }
  return url($path, array('absolute' => TRUE));
}
